const express = require('express')
const path = require('path')
const querystring = require('querystring');
const tiligus_utils =  require('./lib/tiligus-utils')
const app = express()
var shutdown_interval=0, proxy_id=""

app.use('/', express.static(path.join(__dirname, '../www')))

app.get('/api?*', async (req, res) => {
    shutdown_interval=0;
    try{
        var parsed_data=querystring.parse(req.url.substr(5))
        res.json(parsed_data)
    }catch(error){
        console.log(error)
    }
})

async function start_webserver(){
    var available_port = await tiligus_utils.check_empty_ports(30000,50000); // find empty port in range 30000-50000
    if(!available_port) return console.log("No available ports"), process.exit(1)
    app.listen(available_port, async () => {
        proxy_id = await tiligus_utils.create_proxy(available_port,true)
        console.log("server running at port "+available_port)
    })
}
start_webserver()

setInterval(function(){
    shutdown_interval++
    if(shutdown_interval>(60*5)) return console.log("App not called, exiting..."), tiligus_utils.close_proxy(proxy_id,true)
}, 1000)